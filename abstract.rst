.. environment:: abstract

    The Certificate Bundle is designed for use in systems that use the Named Data Networking (NDN) architecture. The Certificate Bundle protocol provides a way to retrieve a set of the certificates needed to authenticate a data packet within one RTT and assures that the certificates for the authentication can be retrieved from the same place as data packet (original data publisher or a managed repo).
